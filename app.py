from flask import Flask, jsonify, request, render_template
import numpy as np
import time
from datetime import datetime

app = Flask(__name__)

# the CADID needs to be unique, so it is pulling the current unix time
CadID = ""
Timestamp = ""
Latitude = 0
Longitude = 0
TodaysDate = ""
BadgeNo = ""

cad_incident = {
    "CadId":CadID,
    "CallType":"ALRMRBUR",
    "Status":"Category",
    # Needs to reflect endpoint query
    "Timestamp":Timestamp,
    "Description":"ALARM BURGLARY-RESIDENTIAL",
    # Needs to reflect endpoint query
    "Latitude":Latitude,
    "Longitude":Longitude,
    "StreetAddress":"00098 VALLEY AVE BRIDGEPORT, CT 06606",
    "CadLinkUrl":"",
    "UnitName":"A27B",
    "UserId":"",
    # Reflect endpoint query
    "BadgeNo":BadgeNo,
    "OtherInfo":"",
    # Reflect todays date
    "InsertedOn":TodaysDate,
    "UpdatedOn":TodaysDate
    }

@app.route('/')
def hello_world():
    # text = (f"DUMMY DATA GENERATOR!"
    #     f"To see the unpopulated cad instance go to:"
    #     f"/cad"
    #     f"To generate dummy CAD data on demo.getaccloud.com in the mikeboe station, go to:"
    #     f"/cad/mikeboe/?badgeno=0001&minrecordingstartedutc=1/1/2020+12:24:56+AM&maxrecordingendedutc=11/20/2020+12:44:56+PM&latitude=41&longitude=-73&radius=50"
    #     f"Alternative query searchs will always return a match."
    #     f"If you do not add query for badgeno, minrecordingstartedutc (timestamp), lat, or long; your result for that element will return 'null'")
    # return render_template('index.html', title="Home", content=cad_incident) 
    return render_template("index.html", text="Home")

@app.route('/cad')
def test_response():
    return jsonify(cad_incident)
    # return render_template('index.html', title="Test Response", content=jsonify(cad_incident)) 

# @app.route('/cad/mikeboe/', methods=['GET'])
# def query_badgenumber():
#     if request.args.get('badgeno', ''):
#         getbadgenumber = request.args.get('badgeno', '')
#         jsonbadgenumber = {"BadgeNo": getbadgenumber}
#         cad_incident.update(jsonbadgenumber)
#         return jsonify(cad_incident)
#     return jsonify({"error": "Page Not Found."}), 404

@app.route('/cad/mycad/', methods=['GET'])
def query_cad_data():

    # grab the requests
    getbadgenumber = request.args.get('badgeno')
    getlat = request.args.get('latitude')
    getlon = request.args.get('longitude')
    getstartdate = request.args.get('minrecordingstartedutc')
    getstartdate = datetime.strptime(getstartdate, "%m/%d/%Y %I:%M:%S %p").strftime("%Y-%m-%dT%H:%M:%S")

    # put them into dict format and update the cad incident
    # CAD ID needs to increment with each call so it is represented by the current unix time stamp
    unixtime = int(time.time())
    getjson = {"CadId": unixtime, "BadgeNo": getbadgenumber, "Latitude": getlat, "Longitude": getlon, "Timestamp": getstartdate}
    cad_incident.update(getjson)

    return jsonify(cad_incident)

if __name__ == '__main__':
    app.run(debug=True)